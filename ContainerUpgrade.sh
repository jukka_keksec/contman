#!/bin/bash
# ContainerMaker
# Arguments should pe passed in this form: 
# ContainerUpgrade.sh binary version
# This will upgrade an existing container (if it's a new install, use ContainerMaker.sh)
# and then link the proper libc (if installed) or prompt the user to
# download it from repos (or to specify the location)

# to prevent confusion
BINNAME=$1
VER=$2

#Stage 1: Determine libc needed
WHATLIBC=$(ldd $1 | grep libc)

# Routine to Upgrade container
# Assuming that contizepkg has done it's job and backed up the previous version
rm /usr/cont/$PKGNAME/version
echo $VER >> /usr/cont/$PKGNAME/version

# take files from extracted /tmp dir and move them in place
cp -rfp /tmp/${pkgtoolsdir} /usr/cont/$PKGNAME/

