#!/bin/bash
# Builds a callscript
# When the container is already created and a binary needs to be called
# rather than creating 10 independent chroots to run one application
# this will be called to execute the binary within the existing one

# This will ensure that all binaries have links to their /execute and deps
# linked in the container's /usr/bin and the path is according to their deps.
# this will be the fun part

