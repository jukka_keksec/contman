#!/bin/bash
# ContainerMaker
# Arguments should pe passed in this form: 
# ContainerMaker.sh binary version
# This will create the container (if it's an upgrade, user ContUpg.sh)
# and then link the proper libc (if installed) or prompt the user to
# download it from repos (or to specify the location)

# to prevent confusion
BINNAME=$1
VER=$2

#Stage 1: Determine libc needed
WHATLIBC=$(ldd $1 | grep libc)

# Routine to create a container (assuming it's NOT an upgrade)
mkdir /usr/cont/${BINNAME}
echo ${VER} >> /usr/cont/${BINNAME}
mv /usr/bin/${BINNAME} /usr/cont/{BINNAME}
#BuildRunscript.sh $WHATLIBC /usr/bin/${BINNAME}
#nb: ^ will create chroot container in said directory (striping binary name ofc)
ln /usr/bin/${BINNAME}/RunMe.sh /usr/bin/${BINNAME}

