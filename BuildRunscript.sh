#!/bin/bash
# This script will determine what all is needed for the binary to function.
# In doing such, it will find the needed libraries on the /usr/cont/lib directory
# and link them in the containerfs. If the library cannot be found, it will be
# downloaded from the providing package, installed to /usr/cont/lib, and then
# will be linked accordingly. 
# See also: healthcheck, which will check all containers
# for the needed libraries and link them if needed to

# Syntax: BuildRunscript.sh 

BIN=$(pwd -l | grep -v "/usr/cont/")
FULL=$(pwd -l)
TARGETA=${FULL}/RunMe.sh
TARGETB=${FULL}/execute

# Ensures that the user doesn't fuck this up
# by using a retarded shell
echo "#!/bin/bash" >> $TARGETA

# And this will create the pseudocontainer
# with con-chroot, a special chroot that
# links things such as the homedir
# and other needed files so that the application
# doesn't otherwise completely shit itself
echo "con-chroot ./ /execute" >> $TARGETA

# Now that TARGETA is done, time to actually link the files

# The grep statement will ensure that we don't
# redundantly link libraries, e.g. ld-linux
NEEDEDLIBS=$(ldd $BIN | grep -v "=>")

for line in $NEEDEDLIBS;
	$location = find /usr/cont/lib -name $line
	if $location -eq "Not found"
		#do subroutine to install the library
		#or prompt the user to specify the location
		#make sure it returns location
	ln $LOCATION ./lib/$line
	# in theory this is all it needs
			
