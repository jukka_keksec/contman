ContMan is the controller for Antithesis' pseudocontainers.

These are nothing novel, they are just a way to allow users to use software written 
for Linux using other libc implementations, such as glibc or ulibc that might not
function correctly under Antithesis's musl implementation. This also provides a way
to manage and segregate older versions of libraries so that /lib and /usr/lib do not 
get cluttered with irrelevent and old versions. A far off yet very hopeful goal of this
software would be that it is some day able to spawn and manage libraries for binaries 
compiled for darwin (via darling) or to help keep wine installations seperate but still
functional.

Finlandia for the world, thanks for all the fish
