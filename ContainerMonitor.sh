#!/bin/bash
# This will monitor all containers
# by listing all processes with con-chroot

PROCESSLIST=$(ps aux | grep con-chroot)

for line in $PROCESSLIST;
	PROCNAME=$(echo $line | grep -v "/usr/cont/")
	PROCNAME=$(echo $PROCNAME | grep -v "/RunMe.sh")
	# now we have the process name
